from re import T
from django.db import models
from django.conf import settings
from django.forms import DateTimeField, DecimalField

USER_MODEL = settings.AUTH_USER_MODEL

# Create your models here.
class ExpenseCategory(models.Model):
    name = models.CharField(max_length=50)
    owner = models.ForeignKey(
        USER_MODEL, related_name="categories", on_delete=models.CASCADE
    )

    def __str__(self):
        return self.name


class Account(models.Model):
    name = models.CharField(max_length=100)
    number = models.CharField(max_length=20)
    owner = models.ForeignKey(
        USER_MODEL, related_name="accounts", on_delete=models.CASCADE
    )

    def __str__(self):
        return self.name


class Receipt(models.Model):
    vendor = models.CharField(max_length=200)
    total = models.DecimalField(max_digits=10, decimal_places=3, null=True)
    tax = models.DecimalField(max_digits=10, decimal_places=3, null=True)
    date = models.DateField(null=True)
    purchaser = models.ForeignKey(
        USER_MODEL, related_name="receipts", on_delete=models.CASCADE
    )
    category = models.ForeignKey(
        ExpenseCategory, related_name="receipts", on_delete=models.CASCADE
    )
    account = models.ForeignKey(
        Account,
        related_name="receipts",
        on_delete=models.CASCADE,
        null=True,
        blank=True,
    )

    def __str__(self):
        return self.vendor
