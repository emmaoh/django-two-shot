from django.forms import ModelForm
from .models import Receipt


class ReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        fields = ["vendor", "total", "tax", "date", "category", "account"]

    def __init__(self, *args, **kwargs):
        user = kwargs.pop("user")
        super(ReceiptForm, self).__init__(*args, **kwargs)
        receipts = Receipt.objects.all()
        category = receipts.values()
        print(category)
        self.fields["category"].queryset = Receipt.objects.filter(
            purchaser=user
        )
        self.fields["account"].queryset = Receipt.objects.filter(
            purchaser=user
        )
