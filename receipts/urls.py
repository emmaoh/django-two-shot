from django.urls import path
from .views import (
    ReceiptCreateView,
    CategoryCreateView,
    AccountCreateView,
    ReceiptListView,
    CategoryListView,
    AccountListView,
)


urlpatterns = [
    path("", ReceiptListView.as_view(), name="home"),
    path("categories/", CategoryListView.as_view(), name="category_list"),
    path("accounts/", AccountListView.as_view(), name="account_list"),
    path(
        "create/",
        ReceiptCreateView.as_view(),
        name="receipt_create",
    ),
    path(
        "categories/create/",
        CategoryCreateView.as_view(),
        name="category_create",
    ),
    path(
        "accounts/create/",
        AccountCreateView.as_view(),
        name="account_create",
    ),
]
