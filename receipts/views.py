from unicodedata import category
from django.shortcuts import render, redirect
from django.views.generic import ListView, TemplateView, CreateView
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import Account, Receipt, ExpenseCategory
from django.urls import reverse_lazy
from .form import ReceiptForm


class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/list.html"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)

    # login_url = "/login/"
    # redirect_field_name = "home"


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    template_name = "receipts/create.html"
    form_class = ReceiptForm

    def get_form_kwargs(self):
        kwargs = super(ReceiptCreateView, self).get_form_kwargs()
        kwargs["user"] = self.request.user
        return kwargs

    def form_valid(self, form):
        receipt = form.save(commit=False)
        receipt.purchaser = self.request.user
        receipt.save()
        form.save_m2m()
        return redirect("home")


class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    template_name = "receipts/accounts/create.html"
    fields = ["name", "number"]

    def form_valid(self, form):
        account = form.save(commit=False)
        account.owner = self.request.user
        account.save()
        return redirect("account_list")

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)


class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = "receipts/accounts/list.html"

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)


class CategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    template_name = "receipts/categories/create.html"
    fields = ["name"]

    def form_valid(self, form):
        category = form.save(commit=False)
        category.owner = self.request.user
        category.save()
        return redirect("category_list")


class CategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = "receipts/categories/list.html"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)
